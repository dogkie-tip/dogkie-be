package com.unq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DogkieApp {

    public static void main(String[] args) {
        SpringApplication.run(DogkieApp.class, args);
    }
}