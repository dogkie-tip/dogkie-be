# Dogkie

![Dogkie](https://www.shareicon.net/data/48x48/2016/02/13/718316_map_512x512.png "Dogkie")

Dogkie is an application which helps dog owners to improve their dog walking experience without getting out of their house.

Dogkie offers a sort of amenities for Owners and also for dog walkers.

### Interaction between both sides

Subscribe for a walk or organize one.

### Tracking

Get real time information about the dog walker location.

### Group or alone

Make the dog go for a walk with a group of dogs or alone

